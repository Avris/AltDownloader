## Avris AltDownloader

A Chrome extension that lets you download images and videos by just Ctrl+Alt+Clicking on them.

### Known limitations

AltDownloader cannot download blob or flash videos, it's just a simple helper to find and download HTML5 videos.

### Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
* **Logo attributions:**
  * https://thenounproject.com/term/keyboard-cmd/1178671/
  * https://thenounproject.com/term/keyboard-alt/1178670/
  * https://thenounproject.com/term/pointer/33351/
  * https://unsplash.com/photos/aJTiW00qqtI
