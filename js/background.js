chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    switch (request['do']) {
        case 'download':
            chrome.downloads.download({
                url: request['url']
            });
            break;
        default:
            console.log('Unrecognised message: ' + request['do']);
    }
});
