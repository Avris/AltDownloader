var isClickInside = function(e, $el) {
    var offset = $el.offset();

    return (e.pageX >= offset.left && e.pageX <= offset.left + $el.outerWidth())
        && (e.pageY >= offset.top && e.pageY <= offset.top + $el.outerHeight());
};

var buildModal = function(links) {
    var out = '<ul class="AltDownloader">';
    out += '';
    if (!Object.keys(links).length) {
        out += '<li>Nothing found</li>';
    }
    Object.keys(links).forEach(function(url) {
        var isVideo = links[url];

        var poster = isVideo === false
            ? url
            : isVideo === true
                ? null
                : isVideo;

        var icon = isVideo ? '[VID]' : '[IMG]'; //? '▶' : '🖼';

        out += '<li>' +
            '<a href="' + url + '" target="_blank" class="AltDownloader-link">' +
                '<span class="img"><img src="' + poster + '"></span>' +
                '<strong>' + icon + '</strong>' +
                ' ' +
                url +
            '</a>' +
        '</li>';
    });

    out += '</ul>';

    return $(out);
};

var getAbsoluteUrl = (function() {
    var a;
    return function(url) {
        if (!a) {
            a = document.createElement('a');
        }
        a.href = url;

        return a.href;
    };
})();

document.body.addEventListener('mouseup', function(e) {
    var $existingModal = $('.AltDownloader');
    if ($existingModal.length && !isClickInside(e, $existingModal)) {
        $existingModal.remove();
    }

    if (!e.ctrlKey || !e.altKey) {
        return;
    }

    var links = {};

    $('img').each(function () {
        if (!isClickInside(e, $(this))) {
            return;
        }

        if (this.attributes['src'] !== undefined) {
            links[getAbsoluteUrl(this.attributes['src'].value)] = false;
        }
    });

    $('video').each(function () {
        if (!isClickInside(e, $(this))) {
            return;
        }

        var poster = this.attributes['poster'] === undefined ? true : this.attributes['poster'].value;

        if (this.attributes['src'] !== undefined) {
            links[getAbsoluteUrl(this.attributes['src'].value)] = poster;
        } else {
            $(this).children('source').each(function () {
                if (this.attributes['src'] !== undefined) {
                    links[getAbsoluteUrl(this.attributes['src'].value)] = poster;
                }
            })
        }
    });

    if (Object.keys(links).length === 1) {
        chrome.runtime.sendMessage({do: 'download', url: Object.keys(links)[0]});
    } else {
        $(this).append(buildModal(links));
    }

    e.stopPropagation();
    e.preventDefault();
}, true);

$('body').on('click', '.AltDownloader-link', function () {
    chrome.runtime.sendMessage({do: 'download', url: $(this).attr('href') });

    return false;
});
